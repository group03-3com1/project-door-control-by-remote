#include <IRremote.h>
#include <Stepper.h>

const int redPin = A2, greenPin = A0, bluePin = A1, relayPin = 2, buzzerPin = 3, IR_RECEIVE_PIN = 7;
const int stepsPerRevolution = 200, openPosition = 400, closedPosition = 0;
int currentPosition = closedPosition;
bool stopMotor = false;

Stepper myStepper(stepsPerRevolution, 8, 9, 10, 11);

void setup() {
  IrReceiver.begin(IR_RECEIVE_PIN, ENABLE_LED_FEEDBACK);
  myStepper.setSpeed(25); // Set the speed of the stepper motor (RPM)
  Serial.begin(9600);
  pinMode(redPin, OUTPUT);
  pinMode(greenPin, OUTPUT);
  pinMode(bluePin, OUTPUT);
  pinMode(relayPin, OUTPUT);
  pinMode(buzzerPin, OUTPUT);
  digitalWrite(relayPin, LOW);
  setColor(HIGH, LOW, LOW); // Red (motor off)
}

void setColor(int red, int green, int blue) {
  digitalWrite(redPin, red);
  digitalWrite(greenPin, green);
  digitalWrite(bluePin, blue);
}

void beep(int frequency, int duration) {
  for (int i = 0; i < duration * 1000L / (2 * frequency); i++) {
    digitalWrite(buzzerPin, HIGH);
    delayMicroseconds(100000L / frequency);
    digitalWrite(buzzerPin, LOW);
    delayMicroseconds(100000L / frequency);
  }
}

void stepAndBeep(int stepDir, int steps) {
  for (int i = 0; i < steps; i++) {
    myStepper.step(stepDir);
    currentPosition += stepDir;
    beep(2000, 5); // Beep with a frequency of 2000 Hz and duration of 5 ms
  }
}

void loop() {
  if (IrReceiver.decode()) {
    int value = IrReceiver.decodedIRData.command;
    Serial.println(value);
    if (value == 224 || value == 144) {
      stopMotor = false;
      digitalWrite(relayPin, HIGH);
      setColor(HIGH, LOW, LOW); // Red (motor running)
      bool opening = (value == 224);
      int targetPosition = opening ? openPosition : closedPosition;
      int stepDir = opening ? 1 : -1;
      Serial.println(opening ? "Opening" : "Closing");
      while ((opening ? currentPosition < targetPosition : currentPosition > targetPosition) && !stopMotor) {
        stepAndBeep(stepDir, 1);
        if (IrReceiver.decode() && IrReceiver.decodedIRData.command == 168) {
          Serial.println("Stop command received");
          stopMotor = true;
          break;
        }
        IrReceiver.resume();
      }
      if (!stopMotor) {
        currentPosition = targetPosition;
        setColor(LOW, HIGH, LOW); // Green (motor stopped)
        beep(200, 5); // Beep to indicate completion
      }
    } else if (value == 168) {
      Serial.println("Stop command received");
      stopMotor = true;
      digitalWrite(relayPin, LOW);
      setColor(LOW, HIGH, LOW); // Green (motor stopped)
      beep(200, 5); // Beep to indicate stop
    }
    IrReceiver.resume();
  }
}
